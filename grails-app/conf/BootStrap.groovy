import com.rs.seguridad.*

class BootStrap {

    def init = { servletContext ->
        Rol rolAdmin = new Rol(authority: 'ROLE_ADMIN').save(flush: true)
        Usuario administrador = new Usuario(username: 'admin', password: '1234', email: 'efren.garcia@rapidsist.com').save(flush: true)
        UsuarioRol.create(administrador, rolAdmin, true)

        assert Usuario.count() == 1
        assert Rol.count() == 1
        assert UsuarioRol.count() == 1
        //Permisos para todos los usuarios
        for (String url in [
                '/',
                '/index',
                '/about',
                '/contact',
                '/systeminfo',
                '/index.gsp',
                '/**/favicon.ico',
                '/**/js/**',
                '/**/css/**',
                '/**/images/**',
                '/login',
                '/login.*',
                '/login/*',
                '/logout',
                '/logout.*',
                '/logout/*',
                '/register/**',
                '/rsCatEmpresa/**',
                '/rsCatGrupoEmpresa/**',
                '/rsCatTipoAsentamiento/**',
                '/rsCatTipoPersona/**',
                '/rsCatTipoTelefono/**',
                '/rsGralAsentamiento/**',
                '/rsGralDelegacionMunicipio/**',
                '/rsGralDomicilio/**',
                '/rsGralEstado/**',
                '/rsGralPais/**',
                '/rsGralTelefono/**',
                '/rsPersona/**'
                ]) {
            new Requestmap(url: url, configAttribute: 'permitAll').save()
        }
        //Permisos para el administrador
        for (String url in [
                '/aclclass/**',
                '/aclentry/**',
                '/aclobjectidentity/**',
                '/aclsid/**',
                '/persistentlogin/**',
                '/registrationcode/**',
                '/requestmap/**',
                '/role/**',
                '/securityinfo/**',
                '/user/**'
        ]) {
            new Requestmap(url: url, configAttribute: 'ROLE_ADMIN').save()
        }
    }
    def destroy = {
    }
}
