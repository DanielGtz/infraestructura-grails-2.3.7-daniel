package com.rs.gral

import com.rs.catalogo.RsCatTipoAsentamiento

/**
 * RsGralAsentamiento
 * A domain class describes the data object and it's mapping to the database
 */
class RsGralAsentamiento {

    String  nombreAsentamiento
    String  codigoPostal
    RsCatTipoAsentamiento 	tipoAsentamiento

    static belongsTo = [delegacionMunicipio : RsGralDelegacionMunicipio]

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }

    static constraints = {
        nombreAsentamiento size:3..100, unique: false,nullable: false, blank: false
        codigoPostal size:5..5, unique: false,nullable: false, blank: false
        tipoAsentamiento()
        delegacionMunicipio()
    }

    String toString() {
        """ESTADO:${delegacionMunicipio?.estado},
		DELEGACION:${delegacionMunicipio},
		${nombreAsentamiento} ${codigoPostal}"""
    }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
