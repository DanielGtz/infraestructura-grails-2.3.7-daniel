package com.rs.gral

import com.rs.catalogo.RsCatTipoPersona
import com.rs.seguridad.Usuario

/**
 * RsPersona
 * A domain class describes the data object and it's mapping to the database
 */
class RsPersona {

    String email
    String apellidoPaterno
    String apellidoMaterno
    String primerNombre
    String segundoNombre
    String nombreAlterno
    String sexo
    String estadoCivil
    Date   fechaNacimiento
    String numeroIdentificacionOficial
    String rfc
    String curp
    String numeroImss

    Usuario  usuario

    static hasMany = [tiposPersona : RsCatTipoPersona, domicilios : RsGralDomicilio]

    static constraints = {
        apellidoPaterno size:3..25, blank: false, unique: false
        apellidoMaterno nullable: true, size:0..25
        primerNombre size:3..25, blank: false, unique: false
        segundoNombre nullable: true, size:0..25
        email email:true, nullable: true, unique: true,  validator: { correo, rsPersona ->
            if (rsPersona.tiposPersona?.claveTipoPersona?.contains('USUARIO')) correo != null }
        sexo(nullable: true, inList:["MASCULINO", "FEMENINO"] )
        estadoCivil nullable: true, inList:["CASADO - BIENES MANCOMUNADOS",
                                            "CASADO - BIENES SEPARADOS",
                                            "DIVORCIADO",
                                            "SOLTERO",
                                            "UNION LIBRE",
                                            "VIUDO"]
        fechaNacimiento(nullable:true)
        //telefonos()
        domicilios()
        nombreAlterno nullable: true, size:0..50
        //identificacionOficial nullable: true
        numeroIdentificacionOficial nullable: true
        rfc nullable: true
        curp nullable: true
        //escolaridad  nullable: true
        numeroImss nullable:true, size:3..50
        //entidadNacimiento nullable: true
        //AL MODIFICAR UNA PERSONA SE VALIDA QUE SI TIENE ASIGNADO UN USUARIO LA PERSONA DEBE TENER TIPO DE USUARIO IGUAL A USUARIO
        //EN LA ALTA DE USUARIO SE ASIGNA EN LA CLASE CONTROLADORA UserController
        tiposPersona nullable: true, validator: { tipoPersona, rsPersona ->
            if (rsPersona.usuario) tipoPersona?.claveTipoPersona?.contains('USUARIO') }
        usuario nullable:true, unique: true
        //datosEmpleado nullable: true
        //datosCliente nullable: true
        //referenciaCliente nullable: true
    }

    String toString() {
        "${apellidoPaterno} ${apellidoMaterno ?: ""} ${primerNombre} ${segundoNombre ?: ""}"
    }

    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
