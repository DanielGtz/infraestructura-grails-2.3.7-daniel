<html>

<head>
	<title><g:message code="default.welcome.title" args="[meta(name:'app.name')]"/> </title>
	<meta name="layout" content="kickstart" />
</head>

<body>
<div class="jumbotron text-center">
    <div class="container">
        <h1>Bienvenido a <strong>LITHOtrack</strong></h1>
        <p>A través de esta plataforma, usted podrá ser parte del proceso de trazabilidad que se genera para cada una de las piezas creadas por Litho Formas.</p>
    </div>
</div>

<div class="container">
    <div class="container-fluid">
        <div class="col-md-3">
            <!--Acomoda un grupo de botones verticalmente-->
            <ul class="nav nav-pills nav-stacked">
            <li class="active"><a class="home" href="${createLink(uri: '/')}"><span class="glyphicon glyphicon-home"></span> <g:message code="default.home.label"/></a></li>
            <sec:ifNotLoggedIn>
                <li><g:link controller="login" action="auth"><span class="glyphicon glyphicon-user"> Entrar</span></g:link></li>
            </sec:ifNotLoggedIn>
            <li><a href="#"><span class="glyphicon glyphicon-envelope"> Mensajes</span></a></li>
            </ul>
        </div>

        <div class="col-md-9">
            <div id="myslide" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myslide" data-slide-to="1" class="active"></li>
                    <li data-target="#myslide" data-slide-to="2"></li>
                    <li data-target="#myslide" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="${resource(dir: 'images', file: 'carruselPrincipal/01.jpg')}" />
                    </div>
                    <div class="item">
                        <img src="${resource(dir: 'images', file: 'carruselPrincipal/02.jpg')}" />
                    </div>
                    <div class="item">
                        <img src="${resource(dir: 'images', file: 'carruselPrincipal/03.jpg')}" />
                    </div>
                </div>
                <a href="#myslide" data-slide="prev" class="left carousel-control"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a href="#myslide" data-slide="next" class="right carousel-control"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>

        </div>
    </div>
</div>

</body>

</html>
