<div id="footer">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-4">
                <h4 class="glyphicon glyphicon-map-marker"> LITHOtrack</h4>
            </div>
            <div class="col-md-4">
                <h4 class="glyphicon glyphicon-earphone">Teléfono <small>(55) 5321 1500</small></h4>
            </div>
            <div class="col-md-4">
                <h4 class="glyphicon glyphicon-envelope"> Contacto <small>ventas@lithoformas.com</small></h4>
            </div>
        </div>
    </div>
</div>