<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<g:set var="entityName" value="${message(code: 'requestmap.label', default: 'Requestmap')}"/>
	<title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>

<div class="container">
	<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="panel panel-default">
			  <div class="panel-heading"><h3 class="text-center"><strong>Create Requestmap</strong></h3></div>
			  <div class="panel-body"><g:form action="save" name='requestmapCreateForm'>
			<div class="dialog">
				<div class="row">
					
					<div class="col-md-3"></div>
					<div class="form-group col-md-6 text-center">
	    				<g:message code='requestmap.url.label' default='URL'/>:
	    				<input class="form-control text-center"  name='url' bean="${requestmap}" autocomplete='off' value="${requestmap?.url}"/>
					</div>
				</div>
			    <div class="row">
			    	<div class="col-md-3"></div>
			        	<div class="form-group col-md-6 text-center">
				    		<g:message code='requestmap.configAttribute.label' default='Config Attribute'/>:
				    		<input class="form-control text-center" bean="${requestmap}" name='configAttribute' autocomplete='off' value="${requestmap?.configAttribute}"/>
						</div>
				</div>
				<div class="row">
					<div class="col-md-5"></div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-large btn-success"  elementId='create' form='requestmapCreateForm'><g:message code='default.button.create.label'/>
						</div>	
				</div>
			</div>
			</g:form></div>
			</div>
			
		</div>
</div>

<script>
$(document).ready(function() {
	$('#url').focus();
});
</script>

</body>
</html>
