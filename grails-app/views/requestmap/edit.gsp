<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<g:set var="entityName" value="${message(code: 'requestmap.label', default: 'Requestmap')}"/>
	<title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
	<div class="container">
		<div class="col-md-3"></div>
		<div class="col-md-6">
		  	<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="text-center"><strong>Edit Requestmap</strong></h3>
			  </div>
			  <div class="panel-body"><g:form action='update' name='requestmapEditForm'>
		<g:hiddenField name="id" value="${requestmap?.id}"/>
		<g:hiddenField name="version" value="${requestmap?.version}"/>
		<div class="dialog">
	
			<br/>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 text-center">
				<g:message code='requestmap.url.label' default='URL'/> </button>
					<input type="text" class="form-control text-center" name='url' class='textField' autocomplete='off' bean="${requestmap}" value='${requestmap?.url}'/>
				</div>
			</div>
				<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 text-center">
					<g:message code='requestmap.configAttribute.label' default='Config Attribute'/> </button>
					<input type="text" class="form-control text-center" name='configAttribute' class='textField' autocomplete='off' bean="${requestmap}" value='${requestmap?.configAttribute}'/>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button class="btn btn-large btn-success btn-block" type="submit" elementId='update' form='requestmapEditForm' > <g:message code='default.button.update.label'/> </button>
				</div>	
		
		<g:if test='${requestmap}'>
		
				<div class="col-md-3">	
					<button class="btn btn-large btn-danger btn-block" type="submit" elementId='deleteButton' form='requestmapEditForm' > <g:message code='default.button.delete.label'/> </button>
				</div>	
		</g:if>
			</div>
		</div>	

	</g:form>


	<g:if test='${requestmap}'>
	
	<s2ui:deleteButtonForm instanceId='${requestmap.id}'/>
	</g:if>
</div>
		</div>
	</div>	
</div>

</body>
</html>
