<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<g:set var="entityName" value="${message(code: 'requestmap.label', default: 'Requestmap')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-default">
		  <div class="panel-heading"><h3 class="text-center"><strong>Requestmap Search</strong></h3></div>
		  <div class="panel-body"><g:form action='requestmapSearch' name='requestmapSearchForm'>
			
			<div class="row">
				<div class="col-md-3"></div>
				<div class="form-group col-md-6 text-center">
	    			<g:message code='requestmap.url.label' default='URL'/>:
	    			<input class="form-control text-center"  name='url' autocomplete='off' value='${url}'/>
				</div>
			</div>
	        <div class="row">
		        <div class="col-md-3"></div>
	        	<div class="form-group col-md-6 text-center">
		    		<g:message code='requestmap.configAttribute.label' default='Config Attribute'/>:
		    		<input class="form-control text-center" name='configAttribute' autocomplete='off' value='${configAttribute}'/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-1">
					<button type="submit" class="btn btn-large btn-info" elementId='search' form='requestmapSearchForm'><g:message code='spring.security.ui.search'/>
				</div>	
			</div>
		</g:form></div>

		  <div class="panel-footer"><g:if test='${searched}'>
<%
def queryParams = [url: url, configAttribute: configAttribute]
%>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<table class="table table-bordered">
						<thead>
						<tr>
							<g:sortableColumn property="url" title="${message(code: 'requestmap.url.label', default: 'URL')}" params="${queryParams}"/>
							<g:sortableColumn property="configAttribute" title="${message(code: 'requestmap.configAttribute.label', default: 'Config Attribute')}" params="${queryParams}"/>
						</tr>
						</thead>
						<tbody>
							<g:each in="${results}" status="i" var="requestmap">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							<td><g:link action="edit" id="${requestmap.id}">${fieldValue(bean: requestmap, field: "url")}</g:link></td>
							<td>${fieldValue(bean: requestmap, field: "configAttribute")}</td>
						</tr>
							</g:each>
						</tbody>
					</table>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="paginateButtons">
						<g:paginate total="${totalCount}" params="${queryParams}" />
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
			<div class="row">
				<div class="col-md-6">
					<div class="alert alert-info text-center">
						<s2ui:paginationSummary total="${totalCount}"/>
					</div>
				</div>
			</div>
	</g:if></div>
		</div>
		
	</div>
</div>


<script>
$(document).ready(function() {
	$("#url").focus();
});
</script>

</body>
</html>
