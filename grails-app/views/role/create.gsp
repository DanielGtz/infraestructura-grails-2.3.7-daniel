<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
	<title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>

<div class="container">
	<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="panel panel-default">
			  <div class="panel-heading"><h3 class="text-center"><strong>Create Role</strong></h3></div>
			  <div class="panel-body"><g:form action="save" name='roleCreateForm'>
				<div class="dialog">
				<div class="row">
					<div class="col-md-5"></div>
					<div class="col-md-2 text-center"><g:message code='role.authority.label' default='Authority'/>:</div>
				</div>
				<div class="row">
					<div class="col-md-3"></div>	
 	 				<div class="col-lg-6">
				    	<div class="input-group">
				      	<input type="text" class="form-control 'textField'"  bean="${role}" name='authority' autocomplete='off' value="${role?.authority}"/>
					      <span class="input-group-btn">
					        <button class="btn btn-success" elementId='create' form='roleCreateForm'>
					    	<g:message code='default.button.create.label'/>
					      </span>
					    </div>
					</div>
				</div>
			</div>
			</g:form></div>
			</div>
			
		</div>
</div>

<script>
$(document).ready(function() {
	$('#authority').focus();
});
</script>

</body>
</html>
