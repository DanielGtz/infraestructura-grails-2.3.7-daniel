<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
	<title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>

<g:form action="update" name='roleEditForm'>
<g:hiddenField name="id" value="${role?.id}"/>
<g:hiddenField name="version" value="${role?.version}"/>

<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title text-center"><g:message code="default.edit.label" args="[entityName]"/></h3>
      </div>
      <div class="panel-body">
        <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-6">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#roles" data-toggle="tab"><span class="glyphicon glyphicon-lock"><strong> Roles</strong></span></a></li>
                <li><a href="#usuarios" data-toggle="tab"><span class="glyphicon glyphicon-user"><strong> Usuarios</strong></span></a></li>
            </ul>
        </div>
    </div>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-6">
        <div class="tab-content">
            <div class="tab-pane fade in active" id="roles">
                <p>
                    <div class="input-group col-md-6">
                      <span class="input-group-addon"><g:message code="role.authority.label" default="Authority"/></span>
                      <input name='authority' class="form-control" bean="${role}" value="${role?.authority}">
                    </div>
                </p>
            </div>
            
            <div class="tab-pane fade" id="usuarios">
                <p>
                <g:if test='${users.empty}'>
                    <div class="form-group">
                    <g:message code="spring.security.ui.role_no_users"/>
                </g:if>
                        <g:each var="u" in="${users}">
                        <g:link controller='user' action='edit' id='${u.id}'>${u.username.encodeAsHTML()}</g:link>
                        </g:each>
                    </div>
                </p>
            </div>
        </div>
    </div>    
</div>


<div class="row">
    <div class="col-md-4"></div>
        <div class="col-md-6">
        <div class="col-md-3 well">
            <button class="btn btn-large btn-success btn-block" type="submit" elementId='update' form='roleEditForm' > <g:message code='default.button.update.label'/> </button>
        </div>
        <g:if test='${role}'>
            <div class="col-md-3 well">
                <button class="btn btn-large btn-danger btn-block" type="submit" id='deleteButton' form='roleEditForm' > <g:message code='default.button.delete.label'/> </button>
            </div>
        </g:if>
        </div>
    </div>
  </div>
</div>

</g:form>

    <g:if test='${role}'>    
        <s2ui:deleteButtonForm instanceId='${role.id}'/>
    </g:if>

</div>
</body>
</html>
