<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<title><g:message code='spring.security.ui.role.search'/></title>
</head>

<body>

<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8">

		<div class="panel panel-default">
		  <div class="panel-heading"><h3 class="text-center"><strong>Role Search</strong></h3></div>
		  <div class="panel-body"><g:form action='roleSearch' name='roleSearchForm'>
		
	<div class="row">
		<div class="col-md-5"></div>
		<div class="col-md-2 text-center">
			<g:message code='role.authority.label' default='Authority'/>:
		</div>
	</div>
	<div class="row">
		<div class="col-md-3"></div>
 		 <div class="col-lg-6">
    		<div class="input-group">
     		 <input type="text" class="form-control" name='authority' class='textField' autocomplete='off' value='${authority}'/>
      			<span class="input-group-btn">
        		<button class="btn btn-info btn-large" elementId='search' form='roleSearchForm'>
    			<g:message code='spring.security.ui.search'/>
      			</span>
   			 </div>
 		</div>
	</div>
</g:form>
</div>
		  <div class="panel-footer"><g:if test='${searched}'>

<%
def queryParams = [authority: authority]
%>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<table class="table table-hover">
				<thead>
					<tr>
						<g:sortableColumn property="authority" title="${message(code: 'role.authority.label', default: 'Authority')}" params="${queryParams}"/>
					</tr>
				</thead>

				<g:each in="${results}" status="i" var="role">
				
				<tbody>
					<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
						<td><g:link action="edit" id="${role.id}">${fieldValue(bean: role, field: "authority")}</g:link></td>
					</tr>
					</g:each>
				</tbody>
			</table>	
		</div>
	</div>
	
	<div class="col-md-3"></div>
	<div class="col-d-6">
	<div class="paginateButtons">
		<g:paginate total="${totalCount}" params="${queryParams}" />
	</div>
	</div>

	<div class="alert alert-info text-center">
		<s2ui:paginationSummary total="${totalCount}"/>
	</div>
	</g:if>
</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$("#authority").focus().autocomplete({
		minLength: 2,
		cache: false,
		source: "${createLink(action: 'ajaxRoleSearch')}"
	});
});
</script>

</body>
</html>
