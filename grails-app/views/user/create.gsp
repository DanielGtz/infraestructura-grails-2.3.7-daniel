<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
    <r:require modules="bootstrap"/>
	<g:set var="entityName" value="${message(code: 'user.label', default: 'Usuario')}"/>
	<title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<g:form action="save" name='userCreateForm' class="form-horizontal">

<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title text-center"><g:message code="default.create.label" args="[entityName]"/></h3>
      </div>
      <div class="panel-body">
        <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-5">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#usuarios" data-toggle="tab"><span class="glyphicon glyphicon-user"><strong> Usuarios</strong></span></a></li>
            <li><a href="#roles" data-toggle="tab"><span class="glyphicon glyphicon-lock"><strong> Roles</strong></span></a></li>
        </ul> 
    </div>
</div>
<div class="row">
    <div class="tab-content">
        <div class="tab-pane fade in active" id="usuarios">
            <p>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <label for="username" class="col-md-2 control-label"><g:message code="user.username.label" default="Usuario"/></label>
                    <div class="col-md-3">
                        <input name='username' class="form-control" bean="${user}" value="${user?.username}" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <label for="password" class="col-md-2 control-label"><g:message code="user.password.label" default="Password"/></label>
                    <div class="col-md-3">
                        <input name="password" type="password" class="form-control" bean="${user}" value="${user?.password}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <label for="enabled" class="col-md-2 control-label"><g:message code="user.enabled.label" default="Habilitado"/></label>
                    <div class="col-md-2">
                        <input name="enabled" type="checkbox" bean="${user}" value="${user?.enabled}" />
                    </div>
                </div>
            <div class="form-group">
                <div class="col-md-3"></div>
                <label for="accountExpired" class="col-md-2 control-label"><g:message code="user.accountExpired.label" default="Cuenta Expirada"/></label>
                <div class="col-md-2">
                    <input name="accountExpired" type="checkbox" bean="${user}" value="${user?.accountExpired}" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3"></div>
                <label for="accountLocked" class="col-md-2 control-label"><g:message code="user.accountLocked.label" default="Cuenta Bloqueada"/></label>
                <div class="col-md-2">
                    <input name="accountLocked" type="checkbox" bean="${user}" value="${user?.accountLocked}" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3"></div>
                <label for="passwordExpired" class="col-md-2 control-label"><g:message code="user.passwordExpired.label" default="Password Expirado"/></label>
                <div class="col-md-2">
                    <input name="passwordExpired" type="checkbox" bean="${user}" value="${user?.passwordExpired}" />
                </div>
            </div>
            </p>
        </div>
        <div class="tab-pane fade" id="roles">
            <p>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <table class="table">
                        <g:each var="auth" in="${authorityList}">
                            <tbody>
                                <td><g:checkBox name="${auth.authority}" /></td>
                                <td><g:link controller='role' action='edit' id='${auth.id}'>${auth.authority.encodeAsHTML()}</g:link></td>
                            </tbody>
                        </g:each>
                        </table>
                    </div>
                </div>
            </p>
        </div>
    </div>
</div>
    <div class="container">
        <div class="col-md-3"></div>
        <div class="col-md-5 well">
            <button class="btn btn-lg btn-primary btn-block" type="submit" elementId='create' form='userCreateForm' > <g:message code='default.button.create.label'/> </button>
        </div>
    </div>

</g:form>
      </div>
      <div class="panel-footer">
        
      </div>
    </div>
    

<!--
<s2ui:tabs elementId='tabs' height='375' data="${tabData}">

	<s2ui:tab name='userinfo' height='280'>
		<table>
		<tbody>

			<s2ui:textFieldRow name='username' labelCode='user.username.label' bean="${user}"
                            labelCodeDefault='Username' value="${user?.username}"/>

			<s2ui:passwordFieldRow name='password' labelCode='user.password.label' bean="${user}"
                                labelCodeDefault='Password' value="${user?.password}"/>

			<s2ui:checkboxRow name='enabled' labelCode='user.enabled.label' bean="${user}"
                           labelCodeDefault='Enabled' value="${user?.enabled}"/>

			<s2ui:checkboxRow name='accountExpired' labelCode='user.accountExpired.label' bean="${user}"
                           labelCodeDefault='Account Expired' value="${user?.accountExpired}"/>

			<s2ui:checkboxRow name='accountLocked' labelCode='user.accountLocked.label' bean="${user}"
                           labelCodeDefault='Account Locked' value="${user?.accountLocked}"/>

			<s2ui:checkboxRow name='passwordExpired' labelCode='user.passwordExpired.label' bean="${user}"
                           labelCodeDefault='Password Expired' value="${user?.passwordExpired}"/>
		</tbody>
		</table>
	</s2ui:tab>

	<s2ui:tab name='roles' height='280'>
		<g:each var="auth" in="${authorityList}">
		<div>
			<g:checkBox name="${auth.authority}" />
			<g:link controller='role' action='edit' id='${auth.id}'>${auth.authority.encodeAsHTML()}</g:link>
		</div>
		</g:each>
	</s2ui:tab>

</s2ui:tabs> -->

    

</div><!--container-->
<script>
$(document).ready(function() {
	$('#username').focus();
	<s2ui:initCheckboxes/>
});
</script>

</body>
</html>
