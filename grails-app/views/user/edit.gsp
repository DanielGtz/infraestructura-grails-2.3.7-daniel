<html>
<%@ page import="org.codehaus.groovy.grails.plugins.PluginManagerHolder" %>

<sec:ifNotSwitched>
	<sec:ifAllGranted roles='ROLE_SWITCH_USER'>
	<g:if test='${user.username}'>
	<g:set var='canRunAs' value='${true}'/>
	</g:if>
	</sec:ifAllGranted>
</sec:ifNotSwitched>

<head>
	<meta name='layout' content='springSecurityUI'/>
	<r:require modules="bootstrap"/>
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
	<title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-5 well">
			<h3 class="text-center"><g:message code="default.edit.label" args="[entityName]"/></h3>
		</div>
	</div>

<g:form action="update" name='userEditForm' class="button-style">
<g:hiddenField name="id" value="${user?.id}"/>
<g:hiddenField name="version" value="${user?.version}"/>

	<div class="row">
	    <div class="col-md-3"></div>
	    <div class="col-md-5">
	        <ul class="nav nav-tabs">
	            <li class="active"><a href="#usuarios" data-toggle="tab"><span class="glyphicon glyphicon-user"><strong> Usuarios</strong></span></a></li>
	            <li><a href="#roles" data-toggle="tab"><span class="glyphicon glyphicon-lock"><strong> Roles</strong></span></a></li>
	        </ul> 
	    </div>
	</div>
	<div class="row">
	    <div class="tab-content">
	        <div class="tab-pane fade in active" id="usuarios">
	            <p>
	        	<div class="row">
	                <div class="form-group">
	                    <div class="col-md-3"></div>
	                    <label for="username" class="col-md-2 control-label"><g:message code="user.username.label" default="Usuario"/></label>
	                    <div class="col-md-3">
	                        <input name='username' class="form-control" bean="${user}" value="${user?.username}">
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="form-group">
	                    <div class="col-md-3"></div>
	                    <label for="password" class="col-md-2 control-label"><g:message code="user.password.label" default="Password"/></label>
	                    <div class="col-md-3">
	                        <input name="password" type="password" class="form-control" bean="${user}" value="${user?.password}" required />
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="form-group">
	                    <div class="col-md-3"></div>
	                    <label for="enabled" class="col-md-2 control-label"><g:message code="user.enabled.label" default="Habilitado"/></label>
	                    <div class="col-md-2">
	                        <input name="enabled" type="checkbox" bean="${user}" value="${user?.enabled}" />
	                    </div>
	                </div>
	            </div>
	            <div class="row">
		            <div class="form-group">
		                <div class="col-md-3"></div>
		                <label for="accountExpired" class="col-md-2 control-label"><g:message code="user.accountExpired.label" default="Cuenta Expirada"/></label>
		                <div class="col-md-2">
		                    <input name="accountExpired" type="checkbox" bean="${user}" value="${user?.accountExpired}" />
		                </div>
		            </div>
		        </div>
		        <div class="row"> 
		            <div class="form-group">
		                <div class="col-md-3"></div>
		                <label for="accountLocked" class="col-md-2 control-label"><g:message code="user.accountLocked.label" default="Cuenta Bloqueada"/></label>
		                <div class="col-md-2">
		                    <input name="accountLocked" type="checkbox" bean="${user}" value="${user?.accountLocked}"/>
		                </div>
		            </div>
		        </div>
		        <div class="row">   
		            <div class="form-group">
		                <div class="col-md-3"></div>
		                <label for="passwordExpired" class="col-md-2 control-label"><g:message code="user.passwordExpired.label" default="Password Expirado"/></label>
		                <div class="col-md-2">
		                    <input name="passwordExpired" type="checkbox" bean="${user}" value="${user?.passwordExpired}"/>
		                </div>
		            </div>
		        </div>
		        </p>
		    </div>
	        <div class="tab-pane fade" id="roles">
	            <p>
	                <div class="form-group">
	                    <div class="col-md-3"></div>
	                    <div class="col-md-6">
	                        <table class="table">
	                        <g:each var="entry" in="${roleMap}">
	                            <tbody>
	                                <td><g:checkBox name="${entry.key.authority}" value="${entry.value}"/></td>
	                                <td><g:link controller='role' action='edit' id='${entry.key.id}'>${entry.key.authority.encodeAsHTML()}</g:link></td>
	                            </tbody>
	                        </g:each>
	                        </table>
	                    </div>
	                </div>
	            </p>
	        </div>
	    </div>
	</div>

	<g:if test='${isOpenId}'>
		<table class="table" name='openIds'>
			<g:if test='${user?.openIds}'>
				<g:each var="openId" in="${user.openIds}">
					<td>${openId.url}</td>
				</g:each>
			</g:if>
			<g:else>
			No OpenIDs registered
			</g:else>
		</table>
	</g:if>







</div><!--Container-->
<s2ui:tabs elementId='tabs' height='375' data="${tabData}">
	
	
	

	<g:if test='${isOpenId}'>
	<s2ui:tab name='openIds' height='275'>
	<g:if test='${user?.openIds}'>
		<ul>
		<g:each var="openId" in="${user.openIds}">
		<li>${openId.url}</li>
		</g:each>
		</ul>
	</g:if>
	<g:else>
	No OpenIDs registered
	</g:else>
	</s2ui:tab>
	</g:if>

</s2ui:tabs>

<div class="row">
    <div class="col-md-4"></div>
        <div class="col-md-6">
        <div class="col-md-3 well">
            <button class="btn btn-large btn-success btn-block" type="submit" elementId='update' form='userEditForm' > <g:message code='default.button.update.label'/> </button>
        </div>
        <g:if test='${role}'>
            <div class="col-md-3 well">
                <button class="btn btn-large btn-danger btn-block" type="submit" id='deleteButton' form='userEditForm' > <g:message code='default.button.delete.label'/> </button>
            </div>
        </g:if>
        </div>
        <g:if test='${canRunAs}'>
			<a id="runAsButton">${message(code:'spring.security.ui.runas.submit')}</a>
		</g:if>
</div>

<!--<div style='float:left; margin-top: 10px;'>
<s2ui:submitButton elementId='update' form='userEditForm' messageCode='default.button.update.label'/>

<g:if test='${user}'>
<s2ui:deleteButton />
</g:if>

<g:if test='${canRunAs}'>
<a id="runAsButton">${message(code:'spring.security.ui.runas.submit')}</a>
</g:if>

</div> -->

</g:form>

<g:if test='${user}'>
<s2ui:deleteButtonForm instanceId='${user.id}'/>
</g:if>

<g:if test='${canRunAs}'>
	<form name='runAsForm' action='${request.contextPath}/j_spring_security_switch_user' method='POST'>
		<g:hiddenField name='j_username' value="${user.username}"/>
		<input type='submit' class='s2ui_hidden_button' />
	</form>
</g:if>

<script>
$(document).ready(function() {
	$('#username').focus();

	<s2ui:initCheckboxes/>

	$("#runAsButton").button();
	$('#runAsButton').bind('click', function() {
	   document.forms.runAsForm.submit();
	});
});
</script>


</body>
</html>
