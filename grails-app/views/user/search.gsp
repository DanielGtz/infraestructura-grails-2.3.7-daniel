<html>

<head>
	<meta name='layout' content='springSecurityUI'/>
<title><g:message code='spring.security.ui.user.search'/></title>
</head>

<body>

<div class="container">
	<g:form action='userSearch' name='userSearchForm'>

	<div class="panel panel-default">
	  <div class="panel-heading">
	  	<!--Barra de búsqueda-->
		<div class="row">
			<div class="col-md-3"></div>	
			<div class="col-lg-6">
		    <div class="input-group">
		      <input type="text" class="form-control text-center" name='username' autocomplete='off' value='${username}' placeholder="Username"/>
		      <span class="input-group-btn">
		        <button class="btn btn-success btn-large" elementId='search' form='userSearchForm'/>
	    		<g:message code='spring.security.ui.search'/>
		      </span>
		    </div>
		  </div>
		</div></div>
	  <div class="panel-body"><div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-3"><g:message code='spring.security.ui.search.true'/></div>
			<div class="col-md-3"><g:message code='spring.security.ui.search.false'/></div>
			<div class="col-md-3"><g:message code='spring.security.ui.search.either'/></div>
		</div>
		<div class="row">
			<div class="col-md-3"><g:message code='user.enabled.label' default='Enabled'/>:</div>
			<div class="col-md-3"><g:radioGroup name='enabled' labels="['','','']" values="[1,-1,0]" value='${enabled}'></div>
			<div class="col-md-3"><%=it.radio%></div>
			<div class="col-md-3"></g:radioGroup></div>
		</div>
		<div class="row">
			<div class="col-md-3"><g:message code='user.accountExpired.label' default='Account Expired'/>:</div>
			<div class="col-md-3"><g:radioGroup name='accountExpired' labels="['','','']" values="[1,-1,0]" value='${accountExpired}'></div>
			<div class="col-md-3"><%=it.radio%></div>
			<div class="col-md-3"></g:radioGroup></div>
		</div>
		<div class="row">
			<div class="col-md-3"><g:message code='user.accountLocked.label' default='Account Locked'/>:</div>
			<div class="col-md-3"><g:radioGroup name='accountLocked' labels="['','','']" values="[1,-1,0]" value='${accountLocked}'></div>
			<div class="col-md-3"><%=it.radio%></div>
			<div class="col-md-3"></g:radioGroup></div>
		</div>
		<div class="row">
			<div class="col-md-3"><g:message code='user.passwordExpired.label' default='Password Expired'/>:</div>
			<div class="col-md-3"><g:radioGroup name='passwordExpired' labels="['','','']" values="[1,-1,0]" value='${passwordExpired}'></div>
			<div class="col-md-3"><%=it.radio%></div>
			<div class="col-md-3"></g:radioGroup></div>
		</div>
	</g:form>
</div>
	  <div class="panel-footer"><g:if test='${searched}'>

<%
def queryParams = [username: username, enabled: enabled, accountExpired: accountExpired, accountLocked: accountLocked, passwordExpired: passwordExpired]
%>

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-2"><g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}" params="${queryParams}"/></div>
			<div class="col-md-2"><g:sortableColumn property="enabled" title="${message(code: 'user.enabled.label', default: 'Enabled')}" params="${queryParams}"/></div>
			<div class="col-md-2"><g:sortableColumn property="accountExpired" title="${message(code: 'user.accountExpired.label', default: 'Account Expired')}" params="${queryParams}"/></div>
			<div class="col-md-2"><g:sortableColumn property="accountLocked" title="${message(code: 'user.accountLocked.label', default: 'Account Locked')}" params="${queryParams}"/></div>
			<div class="col-md-2"><g:sortableColumn property="passwordExpired" title="${message(code: 'user.passwordExpired.label', default: 'Password Expired')}" params="${queryParams}"/></div>
			<div class="col-md-1"></div>
		</div>

		<g:each in="${results}" status="i" var="user">
		<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-2"><g:link action="edit" id="${user.id}">${fieldValue(bean: user, field: "username")}</g:link></div>
			<div class="col-md-2"><g:formatBoolean boolean="${user.enabled}"/></div>
			<div class="col-md-2"><g:formatBoolean boolean="${user.accountExpired}"/></div>
			<div class="col-md-2"><g:formatBoolean boolean="${user.accountLocked}"/></div>
			<div class="col-md-2"><g:formatBoolean boolean="${user.passwordExpired}"/></div>
			<div class="col-md-1"></div>
		</div>	
		</g:each>
	<div class="paginateButtons">
		<g:paginate total="${totalCount}" params="${queryParams}" />
	</div>

	<div class="alert alert-info text-center">
		<s2ui:paginationSummary total="${totalCount}"/>
	</div>

	</g:if>
	</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$("#username").focus().autocomplete({
		minLength: 3,
		cache: false,
		source: "${createLink(action: 'ajaxUserSearch')}"
	});
});

<s2ui:initCheckboxes/>

</script>

</body>
</html>
